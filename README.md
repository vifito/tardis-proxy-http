# Tardis Proxy HTTP (EXPERIMENTAL)

A custom proxy server (layer 7) that send traffic to primary server if resource exists, but if the content does not exist, redirects traffic to an alternative server.

## Installation and configuration

Install dependencies:

```bash
git clone https://gitlab.com/vifito/tardis-proxy-http.git
cd tardis-proxy-http
npm install
```

Configure environments variables. Rename `.env.dist` file to `.env`, and edit

| Variable         | Description                                                 |
| ---------------- | ----------------------------------------------------------- |
| NODE_ENV         | Nodejs environment: development or production               |
| BE_PRIMARY       | Primary backend, application check via HEAD http request    |
| BE_FALLBACK      | Alternative backend                                         |
| SERVER_NAME      | Name or IP for proxy server listen (default: 0.0.0.0)       |
| SERVER_PORT      | Port for server listen (default: 80)                        |
| REDIS_URL        | Redis URL, format redis://user:password@host:port/db-number |
| REDIS_PREFIX     | Prefix keys in redis                                        |

## Usage

Execute nodejs script:

```bash
node server.js
# Or node cluster.js
```

## Requirements

* nodejs and npm
* redis server

## Flow

![Flowchart](./docs/flow.png)

## Build docker image

```bash
# Build image
docker build -t vifito/tardis-proxy-http .

# Execute container
docker run -d -p 8080:80 \
  -e "BE_PRIMARY=http://example.com" \
  -e "BE_FALLBACK=http://google.es" \
  -v `pwd`/data:/data \
  -v `pwd`/log:/app/log \
  --name tardis vifito/tardis-proxy-http
```

## Similar projects

* https://github.com/OptimalBits/redbird

## TODO

* Suppport HTTPS frontend
* Filter malicious URL
* Caching support
