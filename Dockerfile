FROM node:alpine

ENV NODE_ENV production
ENV SERVER_NAME "0.0.0.0"
ENV SERVER_PORT 80
ENV BE_PRIMARY "http://localhost" 
ENV BE_FALLBACK "http://www.google.com"

# redis://user:password@host:port/db-number?db=db-number&option=value
ENV REDIS_URL "redis://127.0.0.1:6379/2"
ENV REDIS_PREFIX "prx:"

# Install redis
RUN apk update && apk add --update redis supervisor && \
    rm -rf /var/cache/apk/* && \
    rm -rf /tmp/* && \
    mkdir /data && mkdir -p /var/log/nodejs && \
    chown -R redis:redis /data && \
    sed -i 's#logfile /var/log/redis/redis.log#logfile ""#i' /etc/redis.conf && \
    sed -i 's#daemonize yes#daemonize no#i' /etc/redis.conf && \
    sed -i 's#dir /var/lib/redis/#dir /data#i' /etc/redis.conf && \
    mkdir -p /app

COPY ./server.js ./package.json /app/
COPY ./docker/tardis.ini /etc/supervisor.d/

WORKDIR /app
RUN npm install --only=production

USER root
VOLUME [ "/data" ]
EXPOSE 80
ENTRYPOINT ["supervisord", "--nodaemon"]