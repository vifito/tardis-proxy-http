/**
 * 
 * 
 * Redis: 
 *    ver todas as entradas: KEYS <REDIS_PREFIX>:*
 *    eliminar todas as entradas: FLUSHALL
 *    información das db de redis: INFO keyspace
 *                                 CONFIG GET databases
 * 
 * Xerar certificados autofirmados:
 *  https://www.ibm.com/support/knowledgecenter/en/SSMNED_5.0.0/com.ibm.apic.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html
 * 
 * 
 */
'use strict';

const http = require('http'),
    httpProxy = require('http-proxy'),
    url = require('url'),
    fs = require('fs'),
    path = require('path'),
    axios = require('axios'),
    redis = require('redis'),
    sha1 = require('sha1'),
    accesslog = require('access-log');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const env = process.env.NODE_ENV || 'development';

const logDir = 'log';
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

// :ip - :userID [:clfDate] ":method :url :protocol/:httpVersion" :statusCode :contentLength ":referer" ":userAgent"
// 127.0.0.1 - - [27/Aug/2019:16:10:56 +0200] "GET /~vifito/d8dev/web/ HTTP/1.1" 200 10223 "http://localhost:8000/~vifito/d8dev/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"

const winstonDailyRotateFile = require('winston-daily-rotate-file');

const formatCustom = format.combine(
  format.splat(),
  format.printf(({ level, message, label, timestamp }) => {
    return `${message}`;    
  })
);

const logger = createLogger({
  level: env === 'development' ? 'debug' : 'info',
  format: formatCustom,

  transports: [
    new transports.Console({
      level: 'info'
    }),
    new transports.File({
      filename: 'log/error.log',
      level: 'error'
    }),
    new transports.File({
      filename: 'log/debug.log',
      level: 'debug'
    }),
    new winstonDailyRotateFile({
      filename: 'log/access-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      level: 'info'
    })
  ]
});    

const requestLog = function(req, res, backend) {
  if(backend != undefined) {
    logger.debug(req.url + ' => ' + backend);
  }

  accesslog(req, res, {}, function(msg) {
    logger.info(msg);
  });
};

// Cargar a configuración (ficheiro .env)
require('dotenv').config();

// Redis
var cache = redis.createClient({
  url:    process.env.REDIS_URL,
  prefix: process.env.REDIS_PREFIX
});

// TODO: configuración SSL
var proxy = httpProxy.createProxyServer({
  secure: true,
  xfwd: true,
  changeOrigin: true
});

proxy.on('error', function (err, req, res) {
  logger.error('ERROR: %s', req.url);
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });
 
  res.end('Something went wrong. And we are reporting a custom error message.');
});

function getCustomHeaders(headers) {
  // Incluír sempre as cookies
  var hdrs = {
    "cookie": headers.cookie
  };

  ["authorization", "accept-language"].forEach(it => {
    if(it in headers) {
      hdrs[it] = headers[it];
    }
  });

  return hdrs;
}

var referer, pathname, cacheKey, headers;

var server = http.createServer(
  function(req, res) {
    pathname = req.url;
    headers  = req.headers;

    // Se a petición é POST temos en conta o referrer
    if(req.method == 'POST') {
      referer = url.parse(req.headers.referer);
      pathname = referer.pathname;
    } 

    // key para REDIS
    cacheKey = sha1(pathname);

    // comprobar se existe en Redis KEYS usc:*
    cache.get(cacheKey, function(err, targetHost) {

      if(targetHost != null) {
        logger.debug('REDIS HIT: %s', targetHost);
        // Xa existe en Redis, directamente servir
        proxy.web(req, res, {target: targetHost});

        requestLog(req, res);
      } else {
        // Non existe, comprobar en que backend está
        var reqHeaders = getCustomHeaders(headers);

        const instance = axios.create({
          headers: reqHeaders
        });

        logger.debug('Petición HEAD: %s', pathname);

        instance.head(process.env.BE_PRIMARY + pathname)
          .then(function(response) {
            logger.debug('Response status: %s', response.status);

            // https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
            if([200, 301, 302, 303, 403].includes(response.status)) {
              proxy.web(req, res, { target: process.env.BE_PRIMARY });
              cache.set(cacheKey, process.env.BE_PRIMARY);

              requestLog(req, res, process.env.BE_PRIMARY, cacheKey);
            } else {
              proxy.web(req, res, { target: process.env.BE_FALLBACK });
              cache.set(cacheKey, process.env.BE_FALLBACK);

              requestLog(req, res, process.env.BE_FALLBACK, cacheKey);
            }
          })
          .catch(function(error) {
            logger.debug('Catch error %s', error);

            proxy.web(req, res, { target: process.env.BE_FALLBACK });
            cache.set(cacheKey, process.env.BE_FALLBACK);

            requestLog(req, res, process.env.BE_FALLBACK, cacheKey);
          });
      }    
    });
  }
);

server.listen(process.env.SERVER_PORT, process.env.SERVER_NAME);
console.log("listening on port %s:%d", process.env.SERVER_NAME, process.env.SERVER_PORT);